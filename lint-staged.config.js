module.exports = {
  linters: {
    '**/*. + (js|md|css|scss|json)': [
      'eslint --fix',
      'prettier --write',
      'git add',
    ],
  },
};
